#ifndef ENGINE_H
#define ENGINE_H



#include "utility.h"

#include <basic_types.h>
#include <engine_interface.h>


namespace othello
{

  class OthelloGameEngine : public othello::GameEngineInterface {

      othello::PlayerId current_player = othello::PlayerId::One;
    // GameEngineInterface interface
  public:
    bool initNewGame() override;
    void clearGame() override;
    bool performMoveForCurrentHuman(const othello::BitPos&) override;
    bool legalMovesCheck();

    void                think(const std::chrono::seconds&) override;
    othello::PlayerId   currentPlayerId() const override;
    othello::PlayerType currentPlayerType() const override;
    othello::BitPieces  pieces(const othello::PlayerId&) const override;

    const othello::BitBoard& board() const override;

    void SwapPlayer();
  };

} // namespace othello
#endif   // ENGINE_H
