#include "engine.h"

namespace othello
{

bool OthelloGameEngine::initNewGame()
{
    m_board[0].reset();
    m_board[1].reset();
    m_board[0].set(27);
    m_board[0].set(36);
    m_board[1].set(28);
    m_board[1].set(35);

    current_player = PlayerId::One;

  return true;
}

void OthelloGameEngine::clearGame() {
    m_board.empty();
}

bool OthelloGameEngine::performMoveForCurrentHuman(const BitPos& board_pos)
{
    if(utility::isLegalMove(m_board, current_player, board_pos))
    {
        utility::placeAndFlip(m_board, current_player, board_pos);
        //m_board.at(size_t(current_player)).set(board_pos.value());
        SwapPlayer();
       return true;
    }
    return false;
}

void OthelloGameEngine::SwapPlayer()
{
    if(current_player == othello::PlayerId::One)
    {
        current_player = othello::PlayerId::Two;
    }
    else
    {
         current_player = othello::PlayerId::One;
    }
}

void OthelloGameEngine::think(const std::chrono::seconds& time_limit)
{
    if(currentPlayerType() == othello::PlayerType::Human)
        return;
    if(currentPlayerId() == othello::PlayerId::One)
    {
        m_player_one.obj->think(m_board, currentPlayerId(), time_limit); //obj is an orangemonkey class
        utility::placeAndFlip(m_board,currentPlayerId(), m_player_one.obj->bestMove());
    }
    else if(currentPlayerId() == othello::PlayerId::Two)
    {
        m_player_two.obj->think(m_board, currentPlayerId(), time_limit);
        utility::placeAndFlip(m_board,currentPlayerId(), m_player_two.obj->bestMove());
    }

    current_player = utility::opponent(currentPlayerId());
}

PlayerId OthelloGameEngine::currentPlayerId() const
{
  return current_player;
}

PlayerType OthelloGameEngine::currentPlayerType() const
{
    if(currentPlayerId() == PlayerId::One)
    {
        return m_player_one.type;
    }
    else
    {
        return m_player_two.type;
    }
}

BitPieces OthelloGameEngine::pieces(const PlayerId& player_id) const
{
  return m_board.at(size_t(player_id));
}

const BitBoard& OthelloGameEngine::board() const {return m_board; }

}   // namespace othello
