#ifndef UTILITY_H
#define UTILITY_H

#include <utility_interface.h>

namespace othello::utility
{

    BitPosSet findBorder(const BitBoard& board);
    PlayerId opponent(const PlayerId& player_id);

}   // namespace othello::utility

#endif // UTILITY_H
